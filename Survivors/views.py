from django.contrib.auth.models import User, Group
from rest_framework import viewsets, mixins, generics
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import SurvivorSerializer
from .models import *
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.views.generic.edit import FormView
from .forms import *

class SurvivorRegisterView(FormView):
    template_name = 'index.html'
    form_class = RegisterSurvivorForm
    
class SurvivorViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Survivor.objects.all()
    serializer_class = SurvivorSerializer
    
    
class ProfileList(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'survivors.html'

    def get(self, request):
        queryset = Survivor.objects.all()
        return Response({'survivors': queryset})
        
class SurvivorsList(APIView):

    def get(self, request):
        queryset = Survivor.objects.all()
        serializer = SurvivorSerializer(queryset, many=True)
        return Response(serializer.data)
        
    def post(self, request):
        form = RegisterSurvivorForm(request.POST)
        if form.is_valid():
            water = form.cleaned_data["water_amount"]
            food = form.cleaned_data["food_amount"]
            medication = form.cleaned_data["medication_amount"]
            ammunition = form.cleaned_data["ammunition_amount"]
            inventory = Inventory(water, food, medication, ammunition)
            
            name = form.cleaned_data["name"]
            age = form.cleaned_data["age"]
            gender = form.cleaned_data["gender"]
            latitude = form.cleaned_data["latitude"]
            longitude = form.cleaned_data["longitude"]
            
            inventory = Inventory(water=water, food=food, medication=medication, ammunition=ammunition)
            inventory.save()
            survivor = Survivor(name=name, age=age, gender=gender, latitude=latitude, longitude=longitude, inventory=inventory)
            survivor.save()
            return HttpResponseRedirect(reverse('people',kwargs={'id':survivor.id}))
            
class SurvivorDetails(mixins.ListModelMixin,mixins.UpdateModelMixin,generics.GenericAPIView):
    serializer_class = SurvivorSerializer
    
    lookup_url_kwarg = "id"

    def get_queryset(self):
        id = self.kwargs.get(self.lookup_url_kwarg)
        queryset = Survivor.objects.filter(id = id)
        return queryset
        
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)
    
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)
        
class SurvivorInventoryDetails(APIView):
    
    def get(self, request, id):
        try:
            survivor = Survivor.objects.filter(id = id)[0]
            json_response = {"inventory": survivor.inventory.get_point_specification()}
            return Response(json_response)
        except:
            messages.error(request._request, 'Survivor not found.')
            return HttpResponseRedirect('/')
        
class SurvivorTradeItem(APIView):
    
    def post(self, request, id):
        
        survivor_items = request.POST.get('inputMyItens',"")
        survivor_id = request.POST.get('inputYourId',"")
        target_items = request.POST.get('inputItensToTrade',"")
        
        try:
            survivor = Survivor.objects.filter(id=survivor_id)[0]
            target_survivor = Survivor.objects.filter(id=id)[0]
            trade(survivor, survivor_items, target_survivor, target_items)
            return HttpResponseRedirect(reverse('people_properties',  kwargs={'id':survivor_id}))

        except:
            messages.error(request._request, 'Something went wrong, check the ID and the items.')
            return HttpResponseRedirect('/')
        
class FlagSurvivorInfected(APIView):
    
    def post(self, request, id):
        try:
            Survivor.flag_survivor(id)
            id = str(id)
            return HttpResponseRedirect(reverse('people', kwargs={'id':id}))
        except:
            messages.error(request._request, 'Survivor not found.')
            return HttpResponseRedirect('/')

class ReportInfected(APIView):

    def get(self, request):
        all_people = Survivor.objects.count()
        nun_infecteds = Survivor.objects.filter(flags__gte = 3).count()
        json_response = {"report": {
                            "description": "Average of infected people",
                            "average_infected": nun_infecteds/float(all_people)}}
        return Response(json_response)
    
class ReportNonInfected(APIView):

    def get(self, request):
        nun_survivors = Survivor.objects.filter(flags__lt = 3).count()
        all_people = Survivor.objects.count()
        json_response = {"report": {
                            "description": "Average of non-infected (healthy) people",
                            "average_infected": nun_survivors/float(all_people)}}
        return Response(json_response)

class ReportPeopleInventory(APIView):

    def get(self, request):
        survivors = Survivor.objects.filter(flags__lt = 3)
        all_people = Survivor.objects.all()
        total_items_survivors = sum([survivor.inventory.count() for survivor in survivors]) 
        total_items_all = sum([people.inventory.count() for people in all_people])
        json_response = {"report": {
                            "description": "Average of the quantity of items per person (total and just non-infected)",
                            "average_items_quantity_per_person": total_items_all/float(len(all_people)),
                            "average_items_quantity_per_healthy_person":total_items_survivors/float(len(survivors))}}
        return Response(json_response)
        
class ReportInfectedPoints(APIView):

    def get(self, request):
        infecteds = Survivor.objects.filter(flags__gte = 3)
        total_points = sum([infected.inventory.get_total_points() for infected in infecteds]) 
        json_response = {"report": {
                            "description": "Total points lost in items that belong to infected people",
                            "total_points_lost":total_points}}
        return Response(json_response)
        
class Reports(APIView):

    def get(self, request):
        reports = [reverse('report_infecteds'),reverse('report_non_infecteds'),reverse('report_people_inventory'),reverse('report_infecteds_points')]
        reports = [request.build_absolute_uri(x) for x in reports]
        json_response = {}
        return Response(reports)
        
def trade(survivor, survivor_items, tsurvivor, tsurvivor_items):
    #First, check if survivors is not infected
    if survivor.flags < 3 and tsurvivor.flags < 3:
        #Check if user input is ok
        if Inventory.is_inventory_valid(survivor_items) and Inventory.is_inventory_valid(tsurvivor_items):
            #Check if survivors have the items
            if survivor.inventory.check_inventory_item(survivor_items) and tsurvivor.inventory.check_inventory_item(tsurvivor_items) :
                survivorItems_points = Inventory.list_to_points(survivor_items)
                tsurvivor_points = Inventory.list_to_points(tsurvivor_items)
                #If it has the same point, then trade
                if survivorItems_points == tsurvivor_points:
                    print survivor, tsurvivor
                    survivor.inventory.trade(tsurvivor_items, survivor_items)
                    tsurvivor.inventory.trade(survivor_items, tsurvivor_items)
        
        
