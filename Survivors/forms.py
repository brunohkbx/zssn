from django import forms
from .models import Survivor
from crispy_forms.helper import FormHelper
from crispy_forms.layout import *
from django.core.urlresolvers import reverse

class RegisterSurvivorForm(forms.ModelForm):
    water_amount = forms.CharField(label='Amount of Water', max_length = 2)
    food_amount = forms.CharField(label='Amount of Food', max_length = 2)
    medication_amount = forms.CharField(label='Amount of Medication', max_length = 2)
    ammunition_amount = forms.CharField(label='Amount of Ammunition', max_length = 2)
    
    
    
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.label_class = 'col-sm-2'  
        self.helper.field_class = 'col-sm-8'
        self.helper.form_class = 'form-horizontal'
        self.helper.form_action = 'people/'
        
        self.helper.layout = Layout(
            Fieldset(
                'Dados Pessoais',
                Field('name'),
                Field('age'),
                Field('gender'),
                Field('latitude'),
                Field('longitude'),
                ),
                Fieldset(
                'Inventary Information',
                Field('water_amount'),
                Field('food_amount'),
                Field('medication_amount'),
                Field('ammunition_amount'),
                ),
                Div(
                    Submit('submit', 'Submit', css_class='button white submit-button'), 
                    css_class='col-sm-12 text-center'))
                
                
        super(RegisterSurvivorForm, self).__init__(*args, **kwargs)
        
    class Meta:
        model = Survivor
        #fields = '__all__'
        exclude = ('inventory','flags',)
        