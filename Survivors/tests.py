from django.test import TestCase
from .models import *
from .views import trade

# Create your tests here.
class SurvivorTestCase(TestCase):
    def setUp(self):
        
        inventory = Inventory.objects.create(water=1,food=1,medication=1,ammunition=1)
        inventory2 = Inventory.objects.create(water=10,food=10,medication=10,ammunition=10)
        Survivor.objects.create(name="Teste", age="21", gender="M", latitude=51.5032520, longitude=-0.1278990, flags=0, inventory=inventory)
        Survivor.objects.create(name="T2", age="21", gender="M", latitude=51.5032520, longitude=-0.1278990, flags=0, inventory=inventory2)
        Survivor.objects.create(name="T3", age="21", gender="M", latitude=51.5032520, longitude=-0.1278990, flags=0, inventory=inventory2)
        Survivor.objects.create(name="Infected", age="21", gender="M", latitude=51.5032520, longitude=-0.1278990, flags=5, inventory=inventory2)
    
    def test_survivor_registration(self):
        """Check if survivors creation is okay"""
        self.assertEqual(Survivor.objects.filter(name = 'Teste').count(), 1)
        self.assertEqual(Survivor.objects.filter(name = 'T2').count(), 1)
        self.assertEqual(Survivor.objects.filter(name = 'T3').count(), 1)
    
    def test_inventory_items(self):
        """Check if the number of items is okay"""
        inventory1 = Inventory.objects.create(water=2,food=3,medication=4,ammunition=5)
        inventory2 = Inventory.objects.create(water=15,food=17,medication=23,ammunition=41)
        self.assertEqual(inventory1.count(), 14)
        self.assertEqual(inventory2.count(), 96)
        
    def test_inventory_points(self):
        inventory1 = Inventory.objects.create(water=2,food=3,medication=4,ammunition=5)
        inventory2 = Inventory.objects.create(water=15,food=17,medication=23,ammunition=41)
        self.assertEqual(inventory1.get_total_points(), 30)
        self.assertEqual(inventory2.get_total_points(), 198)
        
    def test_inventory_validation(self):
        """Check if user input is valid"""
        l_items1 = 'water:10;food:20;medication:30;ammunition:59'
        l_items2 = 'water:10'
        l_items3 = 'water:0;food:0;medication:0;ammunition:9'
        l_items4 = 'water:10;food:0;medication:15;ammunition:9'
        
        self.assertEqual(Inventory.is_inventory_valid(l_items1), True)
        self.assertEqual(Inventory.is_inventory_valid(l_items2), False)
        self.assertEqual(Inventory.is_inventory_valid(l_items3), True)
        self.assertEqual(Inventory.is_inventory_valid(l_items4), True)
        
    def test_list_to_inventory(self):
        """ Check if the transformation of string:list_of_items to Inventory is correct """
        l_items1 = 'water:10;food:20;medication:30;ammunition:59'
        inv = Inventory()
        inv.list_to_inventory(l_items1)
        self.assertEqual(inv.count(), 119)
    
    def test_check_survivor_items(self):
        survivor = Survivor.objects.filter(id = 1)[0]
        l_items = 'water:1;food:1;medication:1;ammunition:1'
        
        survivor2 = Survivor.objects.filter(id = 2)[0]
        l_items2 = 'water:10;food:10;medication:10;ammunition:10'
        
        self.assertEqual(survivor.inventory.check_inventory_item(l_items), True)
        self.assertEqual(survivor2.inventory.check_inventory_item(l_items2), True)
    
    def test_trade(self):
        survivor = Survivor.objects.filter(name = "T2")[0]
        survivor2 = Survivor.objects.filter(name = "T3")[0]
        
        l_items1 = 'water:0;food:2;medication:0;ammunition:0'
        l_items2 = 'water:0;food:0;medication:3;ammunition:0'
        
        trade(survivor, l_items1, survivor2, l_items2)
        self.assertEqual(survivor.inventory.get_food(), '8')
        self.assertEqual(survivor.inventory.get_medication(), '13')
        
        self.assertEqual(survivor2.inventory.get_food(), '12')
        self.assertEqual(survivor2.inventory.get_medication(), '7')
        
        #trade 2
        l_items3 = 'water:2;food:0;medication:0;ammunition:0'
        l_items4 = 'water:0;food:0;medication:1;ammunition:6'
    
        trade(survivor, l_items3, survivor2, l_items4)
        
        self.assertEqual(survivor.inventory.get_water(), '8')
        self.assertEqual(survivor.inventory.get_medication(), '14')
        self.assertEqual(survivor.inventory.get_ammunition(), '16')
        
        self.assertEqual(survivor2.inventory.get_water(), '12')
        self.assertEqual(survivor2.inventory.get_medication(), '6')
        self.assertEqual(survivor2.inventory.get_ammunition(), '4')
        
    def test_trade_infected(self):
        """ An infected survivor cannot trade with others"""
        survivor = Survivor.objects.filter(name = "T2")[0]
        infected = Survivor.objects.filter(name = "Infected")[0]
        
        l_items1 = 'water:1;food:0;medication:0;ammunition:0'
        l_items2 = 'water:0;food:0;medication:2;ammunition:0'
        
        trade(survivor, l_items1, infected, l_items2)
        
        self.assertEqual(survivor.inventory.get_water(), '10')
        self.assertEqual(infected.inventory.get_medication(), '10')
        