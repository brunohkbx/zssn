from __future__ import unicode_literals

from django.db import models
from django.db.models import F
import uuid
import re

# Create your models here.

class Inventory(models.Model):
    water = models.CharField(max_length=2, default=0)
    food = models.CharField(max_length=2, default=0)
    medication = models.CharField(max_length=2, default=0)
    ammunition = models.CharField(max_length=2, default=0)
    
    def __str__(self):              # __unicode__ on Python 2
        return "{0} Items".format(int(self.water) + int(self.food) + int(self.medication) + int(self.ammunition)) 
        
    def count(self):
        return int(self.water) + int(self.food) + int(self.medication) + 1* int(self.ammunition)
        
    def get_total_points(self):
        return 4 * int(self.water) + 3 * int(self.food) + 2 * int(self.medication) + 1 * int(self.ammunition)
        
    def get_point_specification(self):
        """ Return a dictionary in the form food:point """
        return {'Water' : 4 * int(self.water), 'Food' : 3 * int(self.food), 'Medication' : 2 * int(self.medication), 'Ammunition' : 1 * int(self.ammunition) }
        
    def get_water(self):
        return self.water
    
    def get_food(self):
        return self.food
        
    def get_medication(self):
        return self.medication
        
    def get_ammunition(self):
        return self.ammunition
        
    @staticmethod
    def is_inventory_valid(list_of_items):
        """ Check if user list of items is in the format water:x;food:x...;"""
        pattern = r'\b(water:\d{1,2};food:\d{1,2};medication:\d{1,2};ammunition:\d{1,2})\b'
        return True if not re.match(pattern,list_of_items) is None else False
    
    def list_to_inventory(self, list_of_items):
        if Inventory.is_inventory_valid(list_of_items):
            items = self.format_items(list_of_items)
            self.water = items['water']
            self.food = items['food']
            self.medication = items['medication']
            self.ammunition = items['ammunition']
    
    @staticmethod        
    def list_to_points(list_of_items):
        """ return the amount of points regarding the list of items """
        items = dict(e.split(':') for e in list_of_items.split(';'))
        water = int(items['water'])
        food = int(items['food'])
        medication = int(items['medication'])
        ammunition = int(items['ammunition'])
        return 4 * water + 3 * food + 2 * medication + 1 * ammunition
    
    def check_inventory_item(self, list_of_items):
        """ Check if some items is in the inventory """
        items = self.format_items(list_of_items)
        water = int(items['water'])
        food = int(items['food'])
        medication = int(items['medication'])
        ammunition = int(items['ammunition'])
        
        return (water <= int(self.water)) and (food <= int(self.food)) and (medication <= int(self.medication)) and (ammunition <= int(self.ammunition))
    
    def format_items(self, list_of_items):
        """ return dict in the format item:amount """
        return dict(e.split(':') for e in list_of_items.split(';'))
        
    def _add_items(self, list_of_items):
        items = self.format_items(list_of_items)
        self.water = "{0}".format(int(self.water) + int(items['water']))
        self.food = "{0}".format(int(self.food) + int(items['food']))
        self.medication = "{0}".format(int(self.medication) + int(items['medication']))
        self.ammunition = "{0}".format(int(self.ammunition) + int(items['ammunition']))
        
    def _remove_items(self, list_of_items):
        items = self.format_items(list_of_items)
        self.water = "{0}".format(int(self.water) - int(items['water']))
        self.food = "{0}".format(int(self.food) - int(items['food']))
        self.medication = "{0}".format(int(self.medication) - int(items['medication']))
        self.ammunition = "{0}".format(int(self.ammunition) - int(items['ammunition']))
        
        
    def trade(self, new_items, old_items):
        """ add the new items in the inventory and remove the olds"""
        self._add_items(new_items)
        self._remove_items(old_items)
        self.save()
        
    
class Survivor(models.Model):
    GENDER_CHOICES = (("M", "Male"), ("F", "Female"))
    name = models.CharField(max_length=20)
    age = models.CharField(max_length=2)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    latitude = models.DecimalField(max_digits=9, decimal_places=7)
    longitude = models.DecimalField(max_digits=9, decimal_places=7)
    inventory = models.ForeignKey(Inventory)
    flags = models.IntegerField(default=0)
    
    @staticmethod
    def flag_survivor(id): 
        Survivor.objects.filter(id=id).update(flags=F('flags') + 1)
        
    def trade(self, myItems, target_survivor_id, targetItems):
        target_survivor = Survivor.objects.filter(id = target_survivor_id)[0]
        
        #check if both player have the items
        if self.inventory.check_inventory_item(myItems) and target_survivor.inventory.check_inventory_item(targetItems):
            myItems_points = Inventory.list_to_points(myItems)
            targetItems_points = Inventory.list_to_points(targetItems)
            if myItems_points == targetItems_points:
                target_survivor.inventory.trade(myItems, targetItems)
                self.inventory.trade(targetItems, myItems)
                
            
    
