from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Survivor
import uuid


class SurvivorSerializer(serializers.ModelSerializer):
    #inc = serializers.StringRelatedField(many=True)
    
    class Meta:
        model = Survivor
        #fields = '__all__'
        #fields = ('name','age')
        exclude = ('inventory',)
        
    