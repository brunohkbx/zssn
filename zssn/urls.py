from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from Survivors import views
from django.views.generic import TemplateView

router = routers.DefaultRouter()
router.register(r'people', views.SurvivorViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^$',views.SurvivorRegisterView.as_view()),
    url(r'^people/$', views.SurvivorsList.as_view()),
    url(r'^people/(?P<id>\d+)/$', views.SurvivorDetails.as_view(), name='people'),
    url(r'^people/(?P<id>\d+)/properties/$', views.SurvivorInventoryDetails.as_view(), name='people_properties'),
    url(r'^people/(?P<id>\d+)/properties/trade_item/$', views.SurvivorTradeItem.as_view(), name='trade_item'),
    url(r'^people/(?P<id>\d+)/report_infection/$', views.FlagSurvivorInfected.as_view(), name='report_people'),
    url(r'^report/$', views.Reports.as_view()),
    url(r'^report/infected/$', views.ReportInfected.as_view(), name='report_infecteds'),
    url(r'^report/non_infected/$', views.ReportNonInfected.as_view(), name='report_non_infecteds'),
    url(r'^report/people_inventory/$', views.ReportPeopleInventory.as_view(), name='report_people_inventory'),
    url(r'^report/infected_points/$', views.ReportInfectedPoints.as_view(), name='report_infecteds_points'),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^docs/', include('rest_framework_docs.urls')),
]